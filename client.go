package librato

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const Operations = "operations"
const OperationsShort = "ops"

type LibratoClient struct {
	Email, Token string
}

// property strings
const (
	// metric keys
	Name   = "name"
	Period = "period"

	// measurement keys
	Value = "value"
	Tags  = "tags"

	MeasurementsPostUrl = "https://metrics-api.librato.com/v1/measurements"
)

type Measurement map[string]interface{}
type Metric map[string]interface{}

type Batch struct {
	Time         int64         `json:"time"`
	Measurements []Measurement `json:"measurements,omitempty"`
}

func (self *LibratoClient) PostMeasurements(batch Batch) (err error) {
	var (
		js   []byte
		req  *http.Request
		resp *http.Response
	)

	if len(batch.Measurements) == 0 {
		return nil
	}

	if js, err = json.Marshal(batch); err != nil {
		return
	}

	if req, err = http.NewRequest("POST", MeasurementsPostUrl, bytes.NewBuffer(js)); err != nil {
		return
	}

	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(self.Email, self.Token)

	if resp, err = http.DefaultClient.Do(req); err != nil {
		return
	}
	defer resp.Body.Close()

	statusCode := resp.StatusCode
	switch statusCode {
	case http.StatusOK, http.StatusCreated, http.StatusAccepted, http.StatusNoContent:
	default:
		var body []byte
		if body, err = ioutil.ReadAll(resp.Body); err != nil {
			body = []byte(fmt.Sprintf("(could not fetch response body for error: %s)", err))
		}
		err = fmt.Errorf("Unable to post to Librato: %d %s %s", statusCode, resp.Status, string(body))
	}

	return
}
