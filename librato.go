package librato

import (
	"fmt"
	"log"
	"math"
	"time"

	"github.com/adfin/go-metrics"
)

type Reporter struct {
	Email, Token string
	Interval     time.Duration
	Registry     metrics.Registry
	intervalSec  int64
}

func NewReporter(registry metrics.Registry, duration time.Duration, email string, token string) *Reporter {
	return &Reporter{email, token, duration, registry, int64(duration / time.Second)}
}

func Librato(registry metrics.Registry, duration time.Duration, email string, token string) {
	NewReporter(registry, duration, email, token).Run()
}

func (self *Reporter) Run() {
	ticker := time.Tick(self.Interval)
	metricsApi := &LibratoClient{self.Email, self.Token}

	for now := range ticker {
		var measurements Batch
		var err error

		if measurements, err = self.BuildRequest(now, self.Registry); err != nil {
			log.Printf("ERROR constructing librato request body %s", err)
			continue
		}

		if err := metricsApi.PostMeasurements(measurements); err != nil {
			log.Printf("ERROR sending measurements to librato %s", err)
			continue
		}
	}
}

func (self *Reporter) BuildRequest(now time.Time, r metrics.Registry) (snapshot Batch, err error) {
	snapshot = Batch{
		// coerce timestamps to a stepping fn so that they line up in Librato graphs
		Time:         (now.Unix() / self.intervalSec) * self.intervalSec,
		Measurements: make([]Measurement, 0),
	}

	r.Each(func(name string, metric interface{}) {
		measurement := Measurement{}
		measurement[Period] = self.Interval.Seconds()
		switch m := metric.(type) {
		case metrics.Counter:
			if m.Count() > 0 {
				measurement[Name] = fmt.Sprintf("%s.%s", name, "count")
				measurement[Value] = float64(m.Count())
				if m.Tags() != nil {
					measurement[Tags] = m.Tags()
					fmt.Println(m.Tags())
				}

				snapshot.Measurements = append(snapshot.Measurements, measurement)
				// Clear the counter;
				m.Clear()
			}
		case metrics.Timer:
			if m.Count() > 0 {
				requestsCount := Measurement{
					Name:  fmt.Sprintf("%s.%s", name, "count"),
					Value: float64(m.Count()),
				}

				responseTime := Measurement{
					Name:  fmt.Sprintf("%s.%s", name, "mean-resp-time"),
					Value: m.Mean() / math.Pow(10, 6), //resp time in ms
				}

				if m.Tags() != nil {
					requestsCount[Tags] = m.Tags()
					responseTime[Tags] = m.Tags()
				}

				snapshot.Measurements = append(snapshot.Measurements, requestsCount, responseTime)
				// Clear the counter;
				m.Clear()
			}
		}
	})
	return
}
